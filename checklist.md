<pre>
2022 Windows Server Checklist  (draft)
_____  Create a video using ffmpeg from Liunx showing most of the following:
         Post video in localhost/video/
#WINDDOWS VM CHECKLIST (VIRTUAL BOX or VMWARE)
TRY TO USE 64 BIT APPLICATIONS
_____ Set VM to bridge.
_____ Set Static ip : Control Panel\Network and Internet\Network Connections
        AM 10.183.11.# / 16  PM 10.183.22.# / 16
_____ Create a "windows" project(repo) in your gitlab.com account.
_____ Add the files from this project(repo). https://gitlab.com/tritechsc/windows
_____ Install MS Visual Studio Code (https://code.visualstudio.com/)
_____ Install Python 64 bit.  Test with your turtle graphics. C:\Program Files\Python39>
_____ Process explorer - upload text output to data folder.
_____ Install and run tcpview - upload text output to the windows project
_____ Change the name of your computer (hostname)
_____ Install 2 pdf viewers (Adobe Acrobat Reader  and your choice)
_____ Install firefox and chrome
_____ Install and run Packettracer
_____ Install and use git ( https://git-scm.com/downloads)
        Make git usable with PowerShell
_____ Install and use atom.io (optional)
_____ Install and use putty (64bit)
_____ Install and use cygwin
_____ Configure Cygwin for ssh gcc, g++ and ssh)
_____ Configure Cygwin to compile c code.
_____ Compile and C code using Powershell 
_____ Install JDK from Oracle 
_____  Configure classpath for JDK. (Deprecated)
_____  Compile and run JAVA,
_____  Enable God Mode,TaskBarIcons,MyComputer,WinVault,Firewall,Network and NetworkedProgrammInstall
Look at the following using file explorer: %appdata%  and %localappdata%
_____ Upload a screenshots of the result of %appdata% to gitlab data
_____ Upload a screenshots of the result of %localappdata% to gitlab data
_____ compmgmt.msc (Screenshot compmgmt.msc showing users and a custom group.)
_____ mmc (Screenshot host mmc with 3 windows users with one user in a custom group.)
_____ Create folder on your desktop and share it.  Have someone add files to that folder.
_____  Install BGInfo.zip and appy the settings to your desktop. (https://docs.microsoft.com/en-us/sysinternals/downloads/bginfo)
_____ Enable OpenSSH and OpenSSH Server 
          Windows-Settings>Apps>
           Manage optional features
           +Add a feature
            (Add OpenSSH Client and OpenSSH Server)
############################################################################
Server client process:
_____ Install fileZilla and connect to an ftp server.
        Probably 10.183.42.42
        Download irfanview (iview.7z) and install it.
Server side processes:
_____ Install one copy of Nginx.  Install the package at c:\nginx
_____ Add a custom html to Nginx document root.
_____ Start a python webserver at ip:8000
_____ Place text and images in a directory "web/files"
        python3 -m http.server -d web 8000
        Notes: https://stackoverflow.com/questions/39801718/how-to-run-a-http-server-which-serves-a-specific-path
</pre>
 C:\Users\cwc\tritechsc\windows\data> 